﻿#include <iostream>
#include <string>
#include <ctime>

#include "DynamicArray.h"

template<typename ...Args>
void Print(Args ...args) {
	(std::cout << ... << args);
}

template<typename ...Args>
void PrintLn(Args ...args) {
	(std::cout << ... << args) << std::endl;
}

class Helper {
public:
	Helper() { std::cout << "Helper::constructor()" << std::endl; }
	~Helper() { std::cout << "Helper::destructor()" << std::endl; }
};

class Animal {
public:
	virtual ~Animal() {}

	virtual void Voice() {
		PrintLn("I am an simple animal");
	}
};

class Dog : public Animal {
public:
	void Voice() override {
		PrintLn("Wow!");
	}
};

class Cat : public Animal {
private:
	Helper helper;
public:
	void Voice() override {
		PrintLn("Meow!");
	}
};

class Cow : public Animal {
public:
	void Voice() override {
		PrintLn("Mouuu!");
	}
};

int main() {
	auto arr = DynamicArray<Animal*>(0, 4);

	arr.Push(new Animal());
	arr.Push(new Dog());
	arr.Push(new Cat());
	arr.Push(new Cow());

	while (!arr.isEmpty()) {
		auto animal = arr.Pop();
		animal->Voice();
		delete animal;
	}
}
