#pragma once

#include <algorithm>

namespace {
	unsigned int NotSmallerPowerOf2(unsigned int x) {
		--x;
		x |= x >> 1;
		x |= x >> 2;
		x |= x >> 4;
		x |= x >> 8;
		x |= x >> 16;
		return ++x;
	}
}

template<typename T>
class DynamicArray {
private:
	unsigned int allocated;
	unsigned int size;
	T* arr;

	void AllocateTwice() {
		auto newAllocated = allocated == 0 ? 1 : allocated << 1;
		Reallocate(newAllocated);
	}

	void DeAllocateTwice() {
		auto newAllocated = allocated == 0 ? 1 : allocated >> 1;
		Reallocate(newAllocated);
	}

	void Reallocate(const unsigned int newAllocated) {
		auto newArray = new T[newAllocated];
		std::copy(arr, arr + std::min(newAllocated, allocated), newArray);
		allocated = newAllocated;
		delete arr;
		arr = newArray;
	}

public:
	DynamicArray() : allocated(0), size(0) {
		arr = new T[allocated];
	}

	DynamicArray(unsigned int initialSize) : size(initialSize) {
		allocated = NotSmallerPowerOf2(size);
		arr = new T[allocated];
	}

	DynamicArray(unsigned int initialSize, unsigned int preallocate) : size(initialSize) {
		if (preallocate < size) {
			throw new std::invalid_argument("trying to allocate smaller space than needed");
		}
		allocated = NotSmallerPowerOf2(preallocate);
		arr = new T[allocated];
	}

	~DynamicArray() {
		delete arr;
	}

	T& operator[](const unsigned int index) const {
		if (index < 0) {
			throw new std::out_of_range("trying to get index less than zero");
		} else if (index >= size) {
			throw new std::out_of_range("trying to get index greater than size of an array");
		}
		return arr[index];
	}

	void Push(T value) {
		auto curSize = size;
		size++;
		if (size > allocated) {
			AllocateTwice();
		}
		arr[curSize] = value;
	}
	
	T Pop() {
		if (isEmpty()) {
			throw new std::out_of_range("trying to pop empty array");
		}
		size--;
		auto value = arr[size];
		if (size <= (allocated >> 1)) {
			DeAllocateTwice();
		}
		return value;
	}

	unsigned int Allocated() const {
		return allocated;
	}

	void Allocate(unsigned int toAllocate) {
		if (toAllocate < size) {
			throw new std::invalid_argument("trying to allocate smaller space than needed");
		}
		auto newAllocated = NotSmallerPowerOf2(toAllocate);
		if (newAllocated != allocated) {
			Reallocate(newAllocated);
		}
	}

	bool isEmpty() const {
		return size == 0;
	}

	unsigned int Length() const {
		return size;
	}

	void Length(unsigned int newSize) {
		if (newSize < 0) {
			throw new std::invalid_argument("trying to set size less than 0");
		}
		size = newSize;
		auto newAllocated = NotSmallerPowerOf2(size);
		if (newAllocated != allocated) {
			Reallocate(newAllocated);
		}
	}
};
